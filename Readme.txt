Autodesk® Revit® Preview plugin 1.0.0.3 for Total Commander
===========================================================

 * License:
-----------

This software is released as freeware.


 * Disclaimer:
--------------

This software is provided "AS IS" without any warranty, either expressed or
implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not be
liable for any special, incidental, consequential or indirect damages due to
loss of data or any other reason.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.
The default configuration file revit.ini is initialized during the very first
plugin usage.


 * Description:
---------------

Autodesk Revit stores a preview PNG for Revit files.

The Autodesk Revit Preview plugin shows the embedded preview thumbnail of
Revit files. It can also be used for the thumbnail view of Total Commander >= 6.5.

Extracting the preview thumbnail does not require Autodesk Revit to be
installed.

The default background color can be set in section [Autodesk Revit Preview Settings]
of revit.ini.

The optional status bar displays the Revit Build information.


 * ChangeLog:
-------------

 o Version 1.0.0.3 (07.07.2019)
   - minor code improvements
 o Version 1.0.0.2 (16.05.2018)
   - added optional status bar
 o Version 1.0.0.1 (04.05.2018)
   - first build


 * References:
--------------

 o LS-Plugin Writer's Guide by Christian Ghisler
   - http://ghisler.fileburst.com/lsplugins/listplughelp2.1.zip


 * Trademark and Copyright Statements:
--------------------------------------

 o Autodesk and Revit are registered trademarks or trademarks of Autodesk,
   Inc./Autodesk Canada Co.
   - http://www.autodesk.com/revit
 o Total Commander is Copyright © 1993-2019 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net